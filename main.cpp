#ifndef MBED_TEST_MODE
#include "mbed.h"

int main()
{
    DigitalOut led(LED1);

    while (true) {
        led = !led;           
        printf("Blinky\r\n");
        ThisThread::sleep_for(100);
    }
}
#endif /* MBED_TEST_MODE */
