# kbd1-fw

## How to use

### Dependencies

#### Software

* openocd : probably exists as a package from your package manager
* [gcc arm toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)
* [mbedOS](https://os.mbed.com/docs/mbed-os/v6.2/build-tools/install-and-set-up.html)

#### Hardware

* Bluepill
* stlink v2

### Setup mbedOS

#### Clone

Clone the repository using:

```
mbed import git@github.com:tripathiu/kbd1-fw.git
cd kbd1-fw
```

or

```
git clone git@github.com:tripathiu/kbd1-fw.git
cd kbd1-fw
mbed deploy
mbed new .
```

#### Configure

Then, configure your toolchain as per the mbed os website.

```
mbed config --global GCC_ARM_PATH /path/to/gcc-arm
```

Configure your target and toolchain

```
mbed target BLUEPILL_F103C8
```

#### Compile

Then to compile

```
mbed compile
```

#### Flash

Run with the target connected

```
./scripts/flash.sh
```


### Visual Studio Code

* Use [Microsoft C++ extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools) for best results. 

* Generate mbedOS config for VS Code: `mbed export -i vscode_gcc_arm -m BLUEPILL_F103C8`
